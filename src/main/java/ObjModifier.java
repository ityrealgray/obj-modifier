import java.nio.FloatBuffer;

public class ObjModifier {

    private static final int IS_AT_AXIS_X = 0;
    private static final int IS_AT_AXIS_Y = 1;
    private static final int IS_AT_AXIS_Z = 2;
    private static final int NUMBER_OF_AXIS = 3;

    public static void centerVertices(FloatBuffer buf) {

        float[] vectorsFloatArr = new float[buf.capacity()];

        while (buf.hasRemaining()) {
            vectorsFloatArr[buf.position()] = buf.get();
        }

        float maxX = vectorsFloatArr[IS_AT_AXIS_X],
                maxY = vectorsFloatArr[IS_AT_AXIS_Y],
                maxZ = vectorsFloatArr[IS_AT_AXIS_Z],
                minX = vectorsFloatArr[IS_AT_AXIS_X],
                minY = vectorsFloatArr[IS_AT_AXIS_Y],
                minZ = vectorsFloatArr[IS_AT_AXIS_Z];

        for (int index = 0; index < vectorsFloatArr.length; index++) {
            var coordinate = vectorsFloatArr[index];

            switch (index % NUMBER_OF_AXIS) {
                case IS_AT_AXIS_X:
                    if (coordinate > maxX) {
                        maxX = coordinate;
                    } else if (coordinate < minX) {
                        minX = coordinate;
                    }
                    break;
                case IS_AT_AXIS_Y:
                    if (minY > coordinate) {
                        minY = coordinate;
                    } else if (coordinate > maxY) {
                        maxY = coordinate;
                    }
                    break;
                case IS_AT_AXIS_Z:
                    if (coordinate > maxZ) {
                        maxZ = coordinate;
                    } else if (coordinate < minZ) {
                        minZ = coordinate;
                    }
                    break;
                default:
                    break;
            }
        }

        float reposVectorX = getRepositionVectorByAxis(maxX, minX),
                reposVectorY = getRepositionVectorByAxis(maxY, minY),
                reposVectorZ = getRepositionVectorByAxis(maxZ, minZ);

        for (int index = 0; index < vectorsFloatArr.length; index++) {

            switch (index % NUMBER_OF_AXIS) {
                case IS_AT_AXIS_X:
                    vectorsFloatArr[index] += reposVectorX;
                    break;
                case IS_AT_AXIS_Y:
                    vectorsFloatArr[index] += reposVectorY;
                    break;
                case IS_AT_AXIS_Z:
                    vectorsFloatArr[index] += reposVectorZ;
                    break;
                default:
                    break;
            }
        }

        buf.rewind();
        buf.put(vectorsFloatArr);
    }

    private static float getRepositionVectorByAxis(float maxInAxis, float minInAxis) {
        return -(minInAxis - maxInAxis) / 2 - maxInAxis;
    }
}
